function solve(int N, double E, int[] C);
function elemento(int i) -> int;

main {
	var int[] C;
	var double E;
	var int N;
	var int[] result;

	read N;
	read E;

	var double ans;

	alloc C : N;

	for (u:N)
	{
		read C[u];
	}

	call solve(N, E, C) ;

	for (i:N) {
    var int ans;
    call elemento(i) -> ans;
    write ans;
  }
}
