import random
import cmath
from turingarena import *

def evaluate(algorithm):

	for _ in range(1, 10):
		N = random.randint(2,50)
		E = 1000.00

		A = [
			random.randint(-100, 100)
    		for _ in range(0, N)
    	]

		ret = compute(algorithm, N, E, A)

		if fabs(ret - solve(N, B, A)) < pow(10, -6):
			print("Correct")
		else:
			print("wrong")



def compute(algorithm, N, E, C):
	with algorithm.run() as process:
		memory_usage = process.sandbox.get_info().memory_usage
		memory_usage = (memory_usage/1024)/1024
		print(f"memory usage: {memory_usage} MB")
		process.call.solve(N, E, C)
		result = [None] * 100;

		for i in range(0,100):
			result[i] = process.call.elemento(i)
		print(result[i])
		

def solve(N,B,A):
	#calcolo il valore dei bitcoin
	guadagno = 0
	bitcoint = 1
	vi_bitcoin = B

	for i in range(0,N):
		#il valore sta scendendo
		if A[i] < 0 and bitcoin == 1:
			#vendo bitcoin
			guadagno = vi_bitcoin
			bitcoin = 0

			#calcolo il valore del bitcoin
			vi_bitcoin = vi_bitcoin + A[i]
			#se il valore successivo è maggiore di 0 allora compro il valore del  bitcoin
			if A[i+1] > 0 and bitcon == 0:
				guadagno = guadagno - vi_bitcoin
				bitcoin = 1

		elif A[i] > 0 and bitcon == 1:
			vi_bitcoin = vi_bitcoin + A[i]

		#sono arrivato all'ultima iterazione
		elif i == N-1:
			#aspetto il prezzo salga
			if A[i] > 0 and bitcon == 1:
				vi_bitcoin = vi_bitcoin[i] + A[i]
				guadagno = vi_bitcoin
				bitcon = 0
			else:
				guadagno = vi_bitcoin
				vi_bitcoin = vi_bitcoin - A[i]
	return guadano


algorithm = submitted_algorithm()
evaluate(algorithm)
