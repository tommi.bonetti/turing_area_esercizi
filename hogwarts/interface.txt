function raggiungi(int N, int M, int[] A, int[] B, int[] inizio, int[] fine) -> int;

main {

  var int N;
  var int M;
  var int[] A;
  var int[] B;
  var int[] inizio;
  var int[] fine;

  var int ans;

  read N;
  read M;

  alloc A:M;
  alloc B:M;
  alloc inizio:M;
  alloc fine:M;

  for (i:M)
  {
    read A[i];
    read B[i];
    read inizio[i];
    read fine[i];
  }

  call raggiungi(N, M, A, B, inizio, fine) -> ans;
  write ans;

}
