import random
from turingarena import *


def evaluate(algorithm):
    Task0 = True
    Task1 = True
    Task2 = True
    Task3 = True

    #Task0
    #N numero delle sale del castello
    #M numero delle scale del castello

    N = random.randint(3, 10)
    M = random.randint(3, 10)

    A = [None] * M
    B = [None] * M

    for i in range(0, M):
        util = random.sample(range(0, N), 2)
        A[i] = util[0]
        B[i] = util[1]

    inizio = [
        random.randint(0, 10)
        for _ in range(0, M)
    ]

    fine = [
        random.randint(11, 15)
        for _ in range(0, M)
    ]

    ret = compute(algorithm, N, M, A, B, inizio, fine)

    Task0 = Task0 & (ret == solve(N, M, A, B, inizio, fine))

    print(f"Prova casuale: Task0 -> {Task0}")

    #Task1
    for N in range(2,10):
        for M in range(2,15):

          A = [None] * M
          B = [None] * M

          for i in range(0, M):
              util = random.sample(range(0, N), 2)
              A[i] = util[0]
              B[i] = util[1]

          inizio = [
              random.randint(0, 10)
              for _ in range(0, M)
          ]

          fine = [
              random.randint(12, 15)
              for _ in range(0, M)
          ]

          ret = compute(algorithm, N, M, A, B, inizio, fine)

          Task1 = Task1 & (ret == solve(N, M, A, B, inizio, fine))

    print(f"N <= 10 e M <= 15: Task1 -> {Task1}")

    #Task2

    N = random.randint(2, 10)
    M = random.randint(2, 10)

    A = [None] * M
    B = [None] * M

    for i in range(0, M):
        util = random.sample(range(0, N), 2)
        A[i] = util[0]
        B[i] = util[1]

    inizio = [
      random.randint(0, 0)
      for _ in range(0, M)
    ]

    fine = [
      random.randint(5, 5)
      for _ in range(0, M)
    ]

    ret = compute(algorithm, N, M, A, B, inizio, fine)

    Task2 = Task2 & (ret == solve(N, M, A, B, inizio, fine))

    print(f"Le scale iniziano tutte da tempo 0 e scompaiono a tempo n: Task2 -> {Task2}")

    N = random.randint(2, 10)
    M = random.randint(2, 10)

    A = [None] * M
    B = [None] * M

    for i in range(0, M):
        util = random.sample(range(0, N), 2)
        A[i] = util[0]
        B[i] = util[1]

    inizio = [
      random.randint(0, 0)
      for _ in range(0, M)
    ]

    fine = [
      random.randint(3, 10)
      for _ in range(0, M)
    ]

    ret = compute(algorithm, N, M, A, B, inizio, fine)

    Task3 = Task3 & (ret == solve(N, M, A, B, inizio, fine))

    print(f"Le scale iniziano tutte da tempo 0: Task3 -> {Task3}")


def compute(algorithm, N, M, A, B, inizio, fine):
	with algorithm.run() as process:
		memory_usage = process.sandbox.get_info().memory_usage
		memory_usage = (memory_usage/1024)/1024
		print(f"memory usage: {memory_usage} MB")
		return process.call.raggiungi(N, M, A, B, inizio, fine)

def solve(N, M, A, B, inizio, fine):
    limite_max_N = 500000
    limite_max_M = 1000000
    infinito = 1000000000
    vec_archi = [[None] for y in range(limite_max_M + 1)]

    #vec_archi = [0] * (limite_max_M + 1)
    vec_ci_sono_passato = [[None] for y in range(limite_max_M + 1)]

    #vector <pair <int, int> > vec_archi[limite_max_M + 1];
    #vector <int> vec_ci_sono_passato[limite_max_N + 1];

    #array bool
    #checkati[limite_max_M + 1];
    checkati = [0] * (limite_max_M + 1)
    #array int
    #distanza[limite_max_M + 1];
    distanza = [0] * (limite_max_M + 1)

    for i in range(0,M):
        vec_archi[A[i]].append([i, B[i]])
        vec_archi[B[i]].append([i, A[i]])

    for i in range(0,N):
        checkati[i] = False;
        distanza[i] = infinito;

    vec_ci_sono_passato[0].append(0);
    distanza[0] = 0

    for j in range(0, limite_max_N):
        for passato in vec_ci_sono_passato[j]:
            if passato is not None:
                if checkati[passato] is False:
                    for arco in vec_archi[passato]:
                        if arco is not None:
                            primo = arco[0]
                            secondo = arco[1]
                            tempo = max(distanza[passato], inizio[primo]) + 1;
                            if(checkati[secondo] is False and distanza[passato] < fine[primo] and tempo < distanza[secondo]):
                                distanza[secondo] = tempo;
                                vec_ci_sono_passato[tempo].append(secondo);
                    checkati[passato] = True;

    if(distanza[N-1] == infinito):
	    return -1;

    return distanza[N-1];

algorithm = submitted_algorithm()
evaluate(algorithm)
