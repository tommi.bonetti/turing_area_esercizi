#include <vector>
using namespace std;

int raggiungi(int N, int M, int A[], int B[], int inizio[], int fine[]) {
    const int limite_max_N = 500000;
    const int limite_max_M = 1000000;
    int infinito = 1000000000; //limite massimo
    vector <pair <int, int> > vec_archi[limite_max_M + 1];
    vector <int> vec_ci_sono_passato[limite_max_N + 1];
    bool checkati[limite_max_M + 1];
    int distanza[limite_max_M + 1];
    int i,j;
    for(i = 0; i < M; i++){
        vec_archi[A[i]].push_back(make_pair(i, B[i]));
        vec_archi[B[i]].push_back(make_pair(i, A[i]));
    }

    for(i = 0; i < N; i++){
        checkati[i] = false;
        distanza[i] = infinito;
    }

    vec_ci_sono_passato[0].push_back(0);
    distanza[0] = 0;
    //debug: ci vuole un for each!!!
    for(j = 0; j <= limite_max_N; j++){
        for(int passato : vec_ci_sono_passato[j]){
            if(checkati[passato] == false){
                for(const auto& arco: vec_archi[passato]){//debug: scale, tempo, secondo
                    int primo = arco.first;
                    int secondo = arco.second;
                    int tempo = max(distanza[passato], inizio[primo]) + 1;
                    if(checkati[secondo] == false && distanza [passato] < fine[primo] && tempo < distanza[secondo]){
                        distanza[secondo] = tempo;
                        vec_ci_sono_passato[tempo].push_back(secondo);
                    }
                }
                checkati[passato] = true;
            }
        }
    }

    if(distanza[N-1] == infinito){
	    return -1;
    }
    return distanza[N-1];
}
