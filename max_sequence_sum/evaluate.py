import random
import cmath
from turingarena import *

def evaluate(algorithm):

    Task0 = True
    Task1 = True
    Task2 = True
    Task3 = True
    Task4 = True

    #Task0 tutti i numeri della seq negativi
    N = random.randint(2, 30)
    A = [
        random.randint(-100,0)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, A)
    Task0 = Task0 & (ret == solve(N,A))

    #Task1 n <= 30
    N = random.randint(2, 30)
    A = [
        random.randint(-100,0)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, A)
    Task1 = Task1 & (ret == solve(N,A))

    #Task2 n <= 100
    N = random.randint(2, 100)
    A = [
        random.randint(-100,0)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, A)
    Task2 = Task2 & (ret == solve(N,A))

    #Task3 n <= 4000
    N = random.randint(2, 4000)
    A = [
        random.randint(-100,0)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, A)
    Task3 = Task3 & (ret == solve(N,A))


    print("Tutti i numeri della sequenza negativi: Task0 ->", Task0)
    print("Numeri della sequenza <= 30: Task1 ->", Task1)
    print("Numeri della sequenza <= 100: Task2 ->", Task2)
    print("Numeri della sequenza <= 4000: Task3 ->", Task3)
    print("Numeri della sequenza <= 100'000: Task4 ->", Task3)


def compute(algorithm, N, A):
	with algorithm.run() as process:
		memory_usage = process.sandbox.get_info().memory_usage
		memory_usage = (memory_usage/1024)/1024
		print(f"memory usage: {memory_usage} MB")
		return process.call.max_seq_sum(N, A)

def solve(N,A):
    max_ending_here = max_so_far = A[0]
    for x in A[1:]:
        max_ending_here = max(x, max_ending_here + x)
        max_so_far = max(max_so_far, max_ending_here)
    return max_so_far


algorithm = submitted_algorithm()
evaluate(algorithm)
