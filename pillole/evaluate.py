import random
import cmath
from turingarena import *

matrix = [[None] * 100 for _ in range(0,100)]
def evaluate(algorithm):
    numeroPilloleIntere = random.randint(2,15)
    ret = compute(algorithm, numeroPilloleIntere)

    if ret == solve(numeroPilloleIntere, 0):
       print("Risposta corretta")
    else:
      print("Risposta sbagliata")

def compute(algorithm, N):
	with algorithm.run() as process:
		memory_usage = process.sandbox.get_info().memory_usage
		memory_usage = (memory_usage/1024)/1024
		print(f"memory usage: {memory_usage} MB")
		return process.call.calcolaSequenze(N)

def solve(numeroPilloleIntere, numeroPilloleMezze):
    if ((numeroPilloleIntere == 1 and numeroPilloleMezze == 0) or (numeroPilloleIntere == 0 and numeroPilloleMezze != 0)):
        return 1
    elif(matrix[numeroPilloleIntere][numeroPilloleMezze] is not None):
        return matrix[numeroPilloleIntere][numeroPilloleMezze]
    elif (numeroPilloleIntere >= 1 and numeroPilloleMezze == 0):
        matrix[numeroPilloleIntere][numeroPilloleMezze] = solve(numeroPilloleIntere - 1, numeroPilloleMezze + 1)
        return matrix[numeroPilloleIntere][numeroPilloleMezze]
    elif (numeroPilloleIntere >= 1 and numeroPilloleMezze is not 0):
        matrix[numeroPilloleIntere][numeroPilloleMezze] = solve(numeroPilloleIntere - 1, numeroPilloleMezze + 1) + solve(numeroPilloleIntere, numeroPilloleMezze - 1)
        return matrix[numeroPilloleIntere][numeroPilloleMezze]


algorithm = submitted_algorithm()
evaluate(algorithm)
