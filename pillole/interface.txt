function calcolaSequenze(int numeroPilloleIntere) -> int;

main {
	var int numeroPilloleIntere;

	read numeroPilloleIntere;

	var int ans;

	call calcolaSequenze(numeroPilloleIntere) -> ans;
	write ans;
}
