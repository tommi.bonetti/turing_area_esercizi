#include <stdio.h>
#include <assert.h>

int matrix[100][100];
int calcolapillole(int numeroPilloleIntere, int numeroPilloleMezze);

//dobbiamo memorizzare i valori
int calcolaSequenze(int numeroPilloleIntere) {
	for(int i = 0; i < 100; i++)
		for(int j = 0; j < 100; j++)
			matrix[i][j] = -1;

	return calcolapillole(numeroPilloleIntere, 0);
}

int calcolapillole(int numeroPilloleIntere, int numeroPilloleMezze) {
	if ((numeroPilloleIntere == 1 && numeroPilloleMezze == 0) || (numeroPilloleIntere == 0 && numeroPilloleMezze != 0)) {
		return 1;
	} else if(matrix[numeroPilloleIntere][numeroPilloleMezze] != -1) {
		return matrix[numeroPilloleIntere][numeroPilloleMezze];
	} else if (numeroPilloleIntere >= 1 && numeroPilloleMezze == 0) {
		return matrix[numeroPilloleIntere][numeroPilloleMezze] = calcolapillole(numeroPilloleIntere - 1, numeroPilloleMezze + 1);
	} else if (numeroPilloleIntere >= 1 && numeroPilloleMezze != 0) {
		return matrix[numeroPilloleIntere][numeroPilloleMezze] = calcolapillole(numeroPilloleIntere - 1, numeroPilloleMezze + 1) + calcolapillole(numeroPilloleIntere, numeroPilloleMezze - 1);
	}
}
