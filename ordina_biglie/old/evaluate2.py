import sys

from math import *
from turingarena import *

random.seed(0) # make it deterministic

def evaluate(algorithm):
    N = random.randint(2,30)

    A = [
        random.randint(0, N-1)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, A)
    A.sort()

    if ret is True:
        print ("Correct")
    else:
        print("Wrong")

#check that the balls are properly aligned
def checkOrderArray(N):
    return all_monotone(N)

def compute(algorithm, N, A):
    with algorithm.run() as process:
        global pesate = 0
        global result = []

        def pesa(p1, p2):
            assert p1 != p2
            pesate += 1

            #check if the number of checks is less than quadratic dimension of array
            if pesate > pow(N,2):
                raise ValueError("More thant quadratic complexity stop execution! Stop execution!")
            elif A[p1] > A[p2]:
                return 1
            elif A[p1] < A[p2]:
                return -1
            assert False

        s = process.call.ordina(N, pesa=pesa)
        for i in range(0,N):
            result[i] = process.call.elemento(i)
        return (checkOrderArray(result), pesate)

task0 = True
task1 = True
task2 = True
task3 = True
task4 = True
task5 = True

for N in [3, 10, 100]:
    (answer_is_correct, number_of_weights) = evaluate_solution(N)
    task0 = task0 & answer_is_correct

for N in [3, 10,100]:
    (answer_is_correct, number_of_weights) = evaluate_solution(N)
    task1 = task1 & answer_is_correct

(answer_is_correct,number_of_weights) = evaluate_solution(7)
task2 = answer_is_correct & (number_of_weights <= 21)

(answer_is_correct,number_of_weights) = evaluate_solution(7)
task3 = answer_is_correct & (number_of_weights <= 5*7*ceil(log(7,2)))

(answer_is_correct,number_of_weights) = evaluate_solution(7)
task4 = answer_is_correct & (number_of_weights <= 7*ceil(log(7,2)))

(answer_is_correct,number_of_weights) = evaluate_solution(7)
task5 = answer_is_correct & (number_of_weights <=7*(ceil(log(7,2)-1)))

print("Task0:", task0, file=sys.stderr)
print("Task1:", task1, file=sys.stderr)
print("Task2:", task2, file=sys.stderr)
print("Task3:", task3, file=sys.stderr)
print("Task4:", task4, file=sys.stderr)
print("Task5:", task5, file=sys.stderr)
