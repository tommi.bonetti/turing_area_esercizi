from turingarena import *
import random
from math import *

random.seed(0) # make it deterministic

def evaluate(algorithm):
    task0 = True
    task1 = True
    task2 = True
    task3 = True
    task4 = True

    for N in [3, 10, 100]:
        A = list(range(0, N))
        random.shuffle(A)
        (ret, number_of_weights) = compute(algorithm, N, A)

        #controlla se ordinato
        task0 = task0 & checkOrderArray(N, A, ret)


        N = 7
        A = list(range(0, N))
        random.shuffle(A)
        ret, pesate = compute(algorithm, N, A)

        result = checkOrderArray(N, A, ret)
        task1 = result & (pesate <= N*3)
        task2 = result & (pesate <= 5*N*ceil(log(N,2)))
        task3 = result & (pesate <= N*ceil(log(N,2)))
        task4 = result & (pesate <=N*(ceil(log(N,2)-1)))

    print("Soluzione corretta: Task0 ->", task0, file=sys.stderr)
    print("Numero pesate <= N*3: Task1 ->", task1, file=sys.stderr)
    print("Numero pesate <= 5*N*log2(N): Task2 ->", task2, file=sys.stderr)
    print("Numero pesate <= N*log2(N): Task3 ->", task3, file=sys.stderr)
    print("Numero pesate <= N*(log2(N)-1): Task4 ->", task4, file=sys.stderr)

#check that the balls are properly aligned
def checkOrderArray(N, A, res):
    return all(A[res[i]] == i for i in range(N))

def compute(algorithm, N, A):
    with algorithm.run() as process:
        pesate = 0
        result = [0] * N

        def pesa(p1, p2):
            assert p1 != p2
            nonlocal pesate
            pesate += 1

            #check if the number of checks is less than quadratic dimension of array
            if pesate > pow(N,2):
                raise ValueError("More thant quadratic complexity stop execution! Stop execution!")
            elif A[p1] > A[p2]:
                return 1
            elif A[p1] < A[p2]:
                return -1
            assert False

        process.call.ordina(N, pesa=pesa)
        for i in range(0,N):
            result[i] = process.call.elemento(i)
        return result, pesate


algorithm = submitted_algorithm()
evaluate(algorithm)
