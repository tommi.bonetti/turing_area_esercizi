//Merge sort
//100% solution, complexity n(log2 n)

int *A;

int pesa(int p1, int p2);

void merge(int a[], int left_low, int left_high, int right_low, int right_high)
{
    int length = right_high-left_low+1;
    int temp[length];
    int left = left_low;
    int right = right_low;
    for (int i = 0; i < length; ++i) {
        if (left > left_high)
            temp[i] = a[right++];
        else if (right > right_high)
            temp[i] = a[left++];
        else if (pesa(a[left],a[right]) <= 0)
            temp[i] = a[left++];
        else
            temp[i] = a[right++];
    }

    for (int i=0; i< length; ++i)
        a[left_low++] = temp[i];
}

void merge_sort(int a[], int low, int high) {
    if (low >= high)                  //Base case: 1 value to sort->sorted
      return;                         //(0 possible only on initial call)
    else {
      int mid = (low + high)/2;       //Approximate midpoint*
      merge_sort(a, low, mid);        //Sort low to mid part of array
      merge_sort(a, mid+1, high);     //Sort mid+1 to high part of array
      merge(a, low, mid, mid+1,high); //Merge sorted subparts of array
    }
}

void ordina(int n) {
  A = new int[n];

  for(int i = 0; i < n; i++) {
    A[i] = i;
  }

  merge_sort(A, 0, n-1);
}

int elemento(int elemento) {
  return A[elemento];
}
