import random
import cmath
from turingarena import *

def evaluate(algorithm):
    Task0 = True;
    Task1 = True;
    Task2 = True;
    Task3 = True;
    Task4 = True;
    Task5 = True;

    '''
    #Task0 N e L casuali
    N = random.randint(2, 30)
    L = random.randint(2, N)
    H = [
        random.randint(0, N-1)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, L, H)
    Task0 = Task0 & (ret == solve(N, L, H))
    '''

    #Task1 N = L
    N = random.randint(2, 10)
    L = N
    H = [
        random.randint(0, N-1)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, L, H)
    Task1 = Task1 & (ret == solve(N, L, H))

    '''
    #Task2 N < 10
    N = random.randint(2, 10)
    L = random.randint(2, N)
    H = [
        random.randint(0, N-1)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, L, H)
    Task2 = Task2 & (ret == solve(N, L, H))


    #Task3 N < 100 e tutti gli H diversi
    N = random.randint(2, 100)
    L = random.randint(2, N)
    H = random.sample(range(1, 100), N)
    random.shuffle(H)


    ret = compute(algorithm, N, L, H)
    Task3 = Task3 & (ret == solve(N, L, H))

    #Task4 N < 1000
    N = random.randint(2, 100)
    L = random.randint(2, N)
    H = [
        random.randint(0, N-1)
        for _ in range(0, N)
    ]

    ret = compute(algorithm, N, L, H)
    Task4 = Task4 & (ret == solve(N, L, H))
    '''
    print("Soluzione corretta con N e L casuali: Task0 ->", Task0, file=sys.stderr)
    print("Soluzione corretta con N=L: Task1 ->", Task1, file=sys.stderr)
    print("Soluzione corretta con N < 5: Task2 ->", Task2, file=sys.stderr)
    print("Soluzione corretta con N < 10: Task3 ->", Task3, file=sys.stderr)
    print("Soluzione corretta con N < 100 Task4 ->", Task4, file=sys.stderr)


def compute(algorithm, N, L, H):
	with algorithm.run() as process:
		memory_usage = process.sandbox.get_info().memory_usage
		memory_usage = (memory_usage/1024)/1024
		return process.call.posiziona(N, L, H)

def solve(N,L,H):
    #N dimensione pile di fogli
    #L dimensione del PC
    #H array contente le pile di fogli
    min = 0
    help = 0
    candidate = 0
    for i in range(0,N):
        #prendo gli elementi dalla posizione i alla i+L
        if (i+L <= N):
            #array temporaneo
            tmp = H[i:i+L]
            #ordiniamo array
            tmp.sort(reverse=True)

            #va bene non dobbiamo fare niente
            if (tmp[0] - tmp[1] == 1):
                return 0
            elif (tmp[0] - tmp[1] == 2):
                candidate = 1
            else:
                while(tmp[0] - tmp[1] is not 1):
                    tmp[0] = tmp[0] - 1
                    tmp[1] = tmp[1] + 1
                    help = help + 1
                candidate = help

        if (min is not None or min > candidate):
            min = candidate

    return min


algorithm = submitted_algorithm()
evaluate(algorithm)
