def posiziona(N, L, H):
  minore = None
  helpTmp = 0
  for i in range(0,N):
      #prendo gli elemtni dalla posizione i alla i+L
      if (i+L <= N):
          #array temporaneo
          tmp = H[i:i+L]
          #ordiniamo array
          tmp = tmp.sort(reverse=True)

          #va bene non dobbiamo fare niente
          if (tmp[0] - tmp[1] == 1):
              candidate = 0
          elif (tmp[0] - tmp[1] == 2):
              candidate = 1
          else:
              while(tmp[0] - tmp[1] != 1):
                  tmp[0] = tmp[0] - 1
                  tmp[1] = tmp[1] + 1
                  helpTmp = helpTmp + 1
              candidate = helpTmp

          if (minore is not None and candidate < minore):
              minore = candidate
          else:
              minore = candidate
  return minore
